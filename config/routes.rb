Rails.application.routes.draw do
  devise_scope :cashiers do
  end
  devise_for :cashiers
    get '/cashiers/sign_out' => 'devise/sessions#destroy'
  devise_for :admins do
    get '/admins/sign_out' => 'devise/sessions#destroy'
  end
  resources :cashiers
  resources :products do
    post 'change_status'
  end
  resources :orders
  resources :order_lines
  resources :homes
  root 'homes#index'
  get 'homes/index'
  get 'homes/show', as: :homes_show
  post '/orders/summary', to: 'orders#new', as: :order_summary #=> 'orders#create', as: :order_summary
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
