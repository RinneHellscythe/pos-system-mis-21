class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :cashier_id
      t.decimal :total_price
      t.decimal :cash

      t.timestamps
    end
  end
end
