class Order < ApplicationRecord
    #belongs_to :cashier
    attr_accessor :cashier_id, :total_price, :cash
    attr_writer :current_step
    has_many :order_lines#, :class_name => "OrderLine"
    accepts_nested_attributes_for :order_lines, 
                                allow_destroy: true,
                                :reject_if => :all_blank
  
    def current_step
        @current_step || steps.first
    end
    def steps
        %w[form summary]
    end
    def next_step
        self.current_step = steps[steps.index(current_step)+1]
    end
    def previous_step
        self.current_step = steps[steps.index(current_step)-1]
    end
    def last_step?
        current_step == steps.last
    end
    def first_step?
        current_step == steps.first
    end
    #def total_price
    #    self[:total_price]
    #end
    #def cash
    #    self[:cash]
    #end
    def cashier_id
        self[:cashier_id]
    end
end
