class OrderLine < ApplicationRecord
    belongs_to :order
    has_many :products
    def price
        self[:price]
    end
    def quantity
        self[:quantity]
    end
end
