class OrdersController < ApplicationController
    before_action :authenticate_cashier!
    def new
        @order = Order.new
        @order.order_lines.build
        #@order.order_lines.each do |question|
        #    question.answers.build
        #end
    end
    def edit
    end
    def update
    end
    def create
        @order = Order.new(order_params)
        @cash = 0.0
        @order.order_lines.each do |n|
            n[:order_id] = @order.id
            @product = Product.find(n.product_id)
            n.price = @product[:price]
            @order[:total_price] ||= 0
            @order[:total_price] += (n.price * n.quantity)
        end
        @cash = @order[:cash]
        @order[:cash] = @order.cash
        @order[:cashier_id] = current_cashier.id
        @order[:date] = Date.today
        @order.save
        redirect_to @order
        #@order.save
        #redirect_to @order
    end
    def index
        @orders = Order.all
    end
    def destroy
        @order = Order.find(params[:id])
        @order.destroy
        redirect_to controller: 'orders'
    end
    def show
        @order = Order.find(params[:id])
    end
    def summary
        @order = Order.new(order_params)
        render '/orders/summary'
    end
    private
    def order_params
        params.require(:order).permit(:cash, :cashier_id, :total_price,  :order_lines_attributes => [ :product_id, :order_id, :quantity, :price ])
    end
end
