class CashiersController < ApplicationController
  before_action :authenticate_admin!
  def new
      @cashier = Cashier.new
  end
  def create
    @cashier = Cashier.new(params[:cashier].permit(:email, :password, :password_confirmation))
    #@cashier.status = true
    #@cashier.errors.full_messages
    Cashier.create(email: @cashier.email, password: @cashier.password, password_confirmation: @cashier.password_confirmation)
    redirect_to @cashier
    #creating new cashier automatically logs in to the cashier
  end
  def sign_up
      @cashier = Cashier.new
  end
  def edit
    @cashier = Cashier.find(params[:id])
  end
  def update
    @cashier = Cashier.find(params[:id])
    if @cashier.update(cashier_params)
      redirect_to @cashier
    else
      render 'edit'
    end
  end
  def destroy
    @cashier = Cashier.find(params[:id])
    @cashier.destroy
    redirect_to cashiers_path
  end
  def show
    @cashier = Cashier.find(params[:id])
  end
  def index
    @cashiers = Cashier.all
  end
  private
    def cashier_params
        params.require(:cashier).permit(:email, :password, :password_confirmation)
    end
end