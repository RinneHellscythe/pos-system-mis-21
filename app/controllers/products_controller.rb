class ProductsController < ApplicationController
    before_action :authenticate_admin!#, only: [:new, :edit, :update, :create, :destroy, :index, :show]
    #layout "application"
    def new
        @product = Product.new
    end
    def edit
        @product = Product.find(params[:id])
    end
    def update
        @product = Product.find(params[:id])
        if @product.update(product_params)
            redirect_to @product
        else
            render 'edit'
        end
    end
    def create
        @product = Product.new(params[:product].permit(:name, :price, :status))
        @product.status = true
        @product.save
        redirect_to @product
    end
    def destroy
        @product = Product.find(params[:id])
        @product.destroy
        redirect_to products_path
    end
    def show
        @product = Product.find(params[:id])
    end
    def index
        @products = Product.all
    end
    def change_status
        @product = Product.find(params[:id])
        if @product[:status] == true
            @product[:status] = false
        else
            @product[:status] = false
        end
    end
    private
        def product_params
            params.require(:product).permit(:name, :price, :status)
        end
end
