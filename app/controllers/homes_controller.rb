class HomesController < ApplicationController
    before_action :authenticate_admin!, only: [:show]
    def index
    end
    def show
        @orders = Order.all
    end
end
